//import styled from "styled-components";

export interface PropStyle {
    blueBackground? : boolean,
    greenColor?: boolean
}


export interface PropInfoColumn {
    consumptionType: string,
    unit: string,
    value: number,
    greenColor?: boolean
}