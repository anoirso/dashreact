

export const STYLE_CONSTRAINTS = {
    widthC: "90%",
    colorInWhiteBox: "#80939d",
    fontSizeInBox: '12px'
}