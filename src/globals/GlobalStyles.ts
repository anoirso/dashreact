import {css} from "styled-components";


export const customCenter = css`
    display: flex;
    justify-content: center;
    align-items: center;
`

