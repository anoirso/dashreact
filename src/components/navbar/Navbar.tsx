import React from 'react'
import { Link } from 'react-router-dom'
import { NavbarContainer, NavbarItem, NavbarWrapper, NavigationList } from './NavbarSC'

const Navbar = () => {
  return (
    <NavbarContainer>
        <NavbarWrapper>
            <NavigationList>
                <NavbarItem>
                    <Link to="/#" className='nav--link'>Yhteenveto</Link>
                </NavbarItem>
                <NavbarItem>
                    <Link to="/#" className='nav--link'>Kulutus</Link>
                </NavbarItem>
                <NavbarItem>
                    <Link to="/#" className='nav--link'>Päästöt</Link>
                </NavbarItem>
            </NavigationList>
        </NavbarWrapper>

    </NavbarContainer>
  )
}

export default Navbar