import styled from "styled-components";
import { customCenter } from "../../globals/GlobalStyles";




export const NavbarContainer = styled.div`
    width: 100%;
    height: 60px;
    background: #023f6e;
    position: sticky;
    ${customCenter}
`;

export const NavbarWrapper = styled.div`
    width: 90%;
    height: 95%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

export const NavbarItem = styled.div`
    height: 100%;
    padding: 0px 5px;
    ${customCenter}
    .nav--link {
        text-decoration: none;
        color: #ffff;
        font-size: 16px;
    }
`;

export const NavigationList = styled.div`
    height: 100%;
    ${customCenter}
    gap:15px;
`;


