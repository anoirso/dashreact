import { ButtonContainer } from "./ButtonSC"

interface ButtonProps {
    children :any,
    onClick? : any
}

export const Button = ({onClick, children}: ButtonProps) => {
  return (
    <ButtonContainer onClick={onClick ? onClick: null}>
        <h2>{children}</h2>
    </ButtonContainer>
  )
}

