import styled from "styled-components";
import { customCenter } from "../../../globals/GlobalStyles";



export const ButtonContainer = styled.button`
    padding: 10px 30px;
    border-radius: 50px;
    outline: none;
    border: none;
    background: #023f6e;
    ${customCenter}
    transition: all 300ms ease-out;
    & > h2 {
        font-size: 14px;
        font-weight: 400;
        color: #ffff;
    }
    &:hover {
        background: #fff;
        & > h2 {
            color: #023f6e;
        }
    }
    
`;
