import styled from "styled-components";
import { customCenter } from "../../globals/GlobalStyles";
import { STYLE_CONSTRAINTS } from "../../globals/GlobalVariables";

const {widthC} = STYLE_CONSTRAINTS


export const SummaryContainer = styled.div`
    width: 100%;
    background: linear-gradient(90deg, rgba(4,85,148,1) 12%, rgba(0,118,207,1) 94%);
    min-height: 260px;
    ${customCenter}
    flex-direction: column;
    overflow-y: visible;
    position: relative;
`;

export const SectionOneConainter = styled.div`
    width: ${widthC};
    ${customCenter}
    justify-content: flex-start;
    
    .welcome--text {
        font-size: 2rem;
        color: #fff;
        font-weight: 400;
       
    }
    .description--text {
        width: 500px;
        color: #fff;
        font-size: 15px;
        margin-left: 8rem;
    }
`;

export const SectionTwoContainer = styled.div`
    width: ${widthC};
    ${customCenter}
    justify-content: flex-start;
    margin-top: 20px;
    gap: 10px;

`;
///////
// Section three related components are in seperate file


