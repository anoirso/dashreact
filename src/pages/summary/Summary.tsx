import { SummaryContainer } from "./SummarySC";
import SectionOne from "./sections/SectionOne";
import SectionTwo from "./sections/SectionTwo";
import SectionThree from "./sections/SectionThree/SectionThree";

const Summary = () => {
  return (
    <SummaryContainer>
      <SectionOne />
      <SectionTwo />
      <SectionThree />
    </SummaryContainer>
  );
};

export default Summary;
