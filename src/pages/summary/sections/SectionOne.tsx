import { SectionOneConainter } from "../SummarySC";

const SectionOne = () => {
  return (
    <SectionOneConainter>
      <h2 className="welcome--text">Tervetuloa Neste Hiilijälki raporttiin!</h2>
      <div className="description--text">
        <p>
          Neste Hiilijalanjälki raportti tekee yrityksesi liikennepolttoaineiden
          ja työkoneiden kulutuksen, päästöjen ja saavutettujen
          päästövähennysten seuraamisesta helppoa.
        </p>
      </div>
    </SectionOneConainter>
  );
};

export default SectionOne;
