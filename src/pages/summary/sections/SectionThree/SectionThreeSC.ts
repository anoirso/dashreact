import styled from "styled-components";
import { customCenter } from "../../../../globals/GlobalStyles";
import { STYLE_CONSTRAINTS } from "../../../../globals/GlobalVariables";
import { PropStyle } from "../../../../globals/Interfaces";

const {widthC, colorInWhiteBox, fontSizeInBox} = STYLE_CONSTRAINTS


export const SectionThreeContainer= styled.div`
  //  background: red;
    position: absolute;
    width: ${widthC};
    min-height: 300px;
    top: 210px;
    ${customCenter}
    align-items: flex-start;
    gap: 10px;
`;

export const RightPart = styled.div`
    flex: 1;
`;

export const LeftPart = styled.div`
    flex: 2;
    ${customCenter}
    flex-direction: column;
    
`;

export const LeftTopPart = styled.div`
    width: 100%;
    ${customCenter}
    justify-content: space-between;
    align-items: flex-start;
    gap:10px;
  
`;


export const BoxTop = styled.div`
    flex: 1;
    border: 1px solid #646a6d;
    //min-height: 200px;
    border-radius: 25px;
    background: ${({blueBackground} : PropStyle) => 
            blueBackground ? '#023f6e' : '#ffff'};
    ${customCenter}
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    color: ${colorInWhiteBox};
    font-size: ${fontSizeInBox};
    padding: 20px 30px;
    & > .title--box {
        width: 100%;
        ${customCenter}
        justify-content: space-between;
        & > h3 {
            font-size: 1.5rem;
            font-weight: 400;
        }
    }
    .--icon {
        //background: #d3ecf8;
        padding: 3px;
        border-radius: 50%;
        color: #023f6e;
        font-size: 25px 10px;
        transition: all 350ms ease-out;
        &:hover {
            background: #d3ecf8;
            opacity: 0.8;

        }

    }
    & > .--description {
        margin-top: 5px;
    }

`;

export const InfoColumnContainer = styled.div`
    width: 100%;
    margin-top: 15px;
    ${customCenter}
    justify-content: space-between;
    & > p {
        width: 60%;
        font-weight: 500;
    }
    & > .--amount {
        width : 30%;
        ${customCenter}
        flex-direction: column;
        align-items: flex-end;
        & > h4 {
            font-size: 1.7rem;
            color: ${({greenColor}: PropStyle) => greenColor && 'green'}
        }

    }
`;

// Breakline might be moved
export const Breakline = styled.div`
    width: 100%;
    height: 1px;
    background: #dadfe2;
    border-radius: 5px;
    margin-top: 15px;
    `
    



//