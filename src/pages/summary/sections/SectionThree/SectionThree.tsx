import {ArrowForwardIos} from '@mui/icons-material';        
import InfoColumn from './sectionComponents/InfoColumn';
import { BoxTop, Breakline, InfoColumnContainer, LeftPart, LeftTopPart, RightPart, SectionThreeContainer } from './SectionThreeSC';

const SectionThree = () => {
  return (
    <SectionThreeContainer>
         <LeftPart>
            <LeftTopPart>
                    <BoxTop>
                        <div className='title--box'>
                        <h3>Kulutus</h3>
                        <ArrowForwardIos 
                            className='--icon'/>
                        </div>
                        <p className='--description'>Lorem ipsum</p>

                         <InfoColumn 
                          consumptionType='Fossiilisten polttoaineiden kokonaiskulutus'
                          unit='Litraa'
                          value={18900}
                         />
                        <Breakline />
                        {/**  
                        <InfoColumnContainer greenColor={true}>
                            <p>
                            Uusiutuvien polttoaineiden kokonaiskulutus 
                            </p>
                            <div className='--amount'>
                                <h4>
                                5350
                                </h4>
                                <p>Litraa</p>
                            </div>
                        </InfoColumnContainer> */}
                        <InfoColumn 
                          consumptionType='Uusiutuvien polttoaineiden kokonaiskulutus'
                          unit='Litraa'
                          value={5350}
                          greenColor={true}
                        />

                    
                    </BoxTop>
                    <BoxTop>
                        
                    </BoxTop>
                </LeftTopPart>
            </LeftPart>
            <RightPart>
            <BoxTop
                blueBackground={true}
            >
                        
            </BoxTop>
            </RightPart>
        </SectionThreeContainer>
  )
}

export default SectionThree