import { PropInfoColumn } from "../../../../../globals/Interfaces";
import { InfoColumnContainer } from "../SectionThreeSC";

const InfoColumn = ({consumptionType, unit, value, greenColor} : PropInfoColumn) => {
  return (
    <InfoColumnContainer greenColor={greenColor}>
      <p>{consumptionType}</p>
      <div className="--amount">
        <h4>{value}</h4>
        <p>{unit}</p>
      </div>
    </InfoColumnContainer>
  );
};

InfoColumn.defaultProps = {
    greenColor: false
}

export default InfoColumn;
