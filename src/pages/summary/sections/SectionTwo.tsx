import { Button } from "../../../components/customComponents/button/Button";
import { SectionTwoContainer } from "../SummarySC";

const SectionTwo = () => {
  return (
    <SectionTwoContainer>
      <Button>Miten Käytän reporttia</Button>
      <Button>Esityssivustolle</Button>
    </SectionTwoContainer>
  );
};

export default SectionTwo;
