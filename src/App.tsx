import { AppContainer, GlobalStyled } from './AppSC';
import Navbar from './components/navbar/Navbar';
import Summary from './pages/summary/Summary';

function App() {
  return (
    <AppContainer>
      <GlobalStyled />
      <Navbar />
      {/* Router will be implemented soon */}
      <Summary  />
      {/* Router will be implemented soon */}
    </AppContainer>
  );
}

export default App;
