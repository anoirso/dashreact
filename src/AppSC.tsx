import styled, {createGlobalStyle, GlobalStyleComponent} from "styled-components";


export const GlobalStyled = createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
        font-family: 'Roboto Condensed', sans-serif;
    }
`;

export const AppContainer = styled.div`
    width: 100%;
    min-width: 890px;
    overflow-y: visible;
`;